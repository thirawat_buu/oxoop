
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player winner;
    private boolean finish = false;
    private Player currentPlayer;
    private int lastcol, lastrow;
    private int count = 0;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println("   1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(" " + (row + 1));
            for (int column = 0; column < table[row].length; column++) {
                System.out.print(" " + table[row][column]);
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.Win();
            playerX.Lose();
        } else {
            playerO.Lose();
            playerX.Win();
        }
    }

    public void checkX() {
        if (table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X') {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        } else if (table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X') {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        } else if (table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O') {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        } else if (table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O') {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        }

    }

    public void showResult() {
        if (finish == true) {
            System.out.println(winner + " Win!!!");
        } else if (count >= 9) {
            finish = true;
            System.out.println("Draw!!");

        }
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }
}
