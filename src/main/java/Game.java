
import java.util.Scanner;

public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    String restart;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            };
            System.out.println("Error: table at row and col it not empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void newGame() {
        System.out.println("Do you want to start new Game?");
        System.out.println("Answer Yes to start, No to stop.");
        restart = kb.next();
        if (restart.equals("Yes")) {
            table = new Table(playerX, playerO);
        } else {
            System.out.println("Bye Bye...");
        }

    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWinner() + " Win!!");
                }
                this.showTable();
                newGame();
            }
            table.switchPlayer();
        }
    }
}
